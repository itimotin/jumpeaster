#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import "Main.h"
//#import "PouNode.h"
#import <CoreMotion/CoreMotion.h>
#import "Constants.h"
#import <AppDelegate.h>
//#import "MainViewController.h"

@interface Game : Main
{
	CGPoint poo_pos;
	ccVertex2F poo_vel;
	ccVertex2F poo_acc;
    CCSprite *rabbit;
	float currentPlatformY;
	int currentPlatformTag, currentCoinTag;
	float currentMaxPlatformStep;
	int currentBonusPlatformIndex;
	int currentBonusType;
	int platformCount;
    int indexPlatformForBonus;
	
	BOOL gameSuspended;
	BOOL pooLookingRight;
    BOOL isFinish, timerIsRunning;
    BOOL forNonIpad;
	BOOL gameOver, isPause;
	int score, scoreCoin;
    
    int currentBackgroundID;
    int tagForParashute;
    float bonusAccelerateBackground;
//    PouNode *tamagochi;
    uint_fast16_t xGlance, yGlance;
    CCSprite *_pauseButton;
//    MainViewController *myView;
    UIAlertView *alertInfo;
    NSMutableArray *highscores;
    double time;
    NSTimer * timer;
//    CCSprite *bonus;
}

//@property (strong) CCTMXTiledMap *tileMap;
@property (strong) CCTMXLayer *background;

+ (CCScene *)scene;
@end
