#import "cocos2d.h"
#import "Main.h"
#import "Constants.h"


@interface Highscores : Main <UITextFieldDelegate>
{
	NSString *currentPlayer;
	int currentScore;
	int currentScorePosition;
	NSMutableArray *highscores;
	UIAlertView *changePlayerAlert;
	UITextField *changePlayerTextField;
    int_fast8_t indexGame;
}
+ (CCScene *)sceneWithScore:(int)lastScore withIdGame:(int_fast8_t)idGame;
- (id)initWithScore:(int)lastScore withIdGame:(int_fast8_t)idGame;
- (void)button1Callback:(id)sender;
@end
