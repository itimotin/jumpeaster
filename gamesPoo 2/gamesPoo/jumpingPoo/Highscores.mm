#import "Highscores.h"
#import "Main.h"
#import "Game.h"
//#import "GamesLabScene.h"
#import "TinyPoo.h"

@interface Highscores (Private)
- (void)loadCurrentPlayer;
- (void)loadHighscores;
- (void)updateHighscores;
- (void)saveCurrentPlayer;
- (void)saveHighscores;
- (void)button2Callback:(id)sender;
- (void)actionButtonBack:(id)sender;
- (void)functionChangePlayerName;
@end


@implementation Highscores

+ (CCScene *)sceneWithScore:(int)lastScore withIdGame:(int_fast8_t)idGame
{
    CCScene *game = [CCScene node];
    //if < 0 inseamna ca e preview Mode
    
    Highscores *layer = [[[Highscores alloc] initWithScore:lastScore withIdGame:idGame] autorelease];
    
    [game addChild:layer];
    
    return game;
}

- (id)initWithScore:(int)lastScore withIdGame:(int_fast8_t)idGame {
    
	if(![super init]) return nil;
	currentScore = lastScore;
    indexGame = idGame;
	[self loadCurrentPlayer];
	[self loadHighscores];
	[self updateHighscores];
	if(currentScorePosition >= 0) {
		[self saveHighscores];
	}

    //backgrounds
    CCSprite *spriteBackground = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_highscore%@.png",IMG_PREFIX]];
    spriteBackground.position = ccp(WIDTH_DEVICE/2, HEIGHT_DEVICE/2);
    [self addChild:spriteBackground z:0];
    
    CCSprite *backgroundTopHighscore = [CCSprite spriteWithFile:[NSString stringWithFormat:@"top_highscore%@.png",MODELButtons]];
    backgroundTopHighscore.position = ccp(WIDTH_DEVICE/2, HEIGHT_DEVICE/2+HEIGHT_DEVICE/11);
    backgroundTopHighscore.scaleY = 0.7;
    [self addChild:backgroundTopHighscore z:2];
    
    //labels with current Score
    if (lastScore>=0) {
        CCLabelTTF *label = [CCLabelTTF labelWithString:@"Your score:" dimensions:CGSizeMake(WIDTH_DEVICE, 50) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:((IS_IPAD)?40:20)];
        label.position = ccp(WIDTH_DEVICE/2, HEIGHT_DEVICE - 40);
        [self addChild:label z:2];
    }

    
 //if < 0 inseamna ca e preview Mode
    CCLabelTTF *labelScore;
    if (lastScore<0) {
       labelScore = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@",((indexGame)?@"Pou Walker":@"Pou Mr.Sky")] dimensions:CGSizeMake(WIDTH_DEVICE, 50) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:((IS_IPAD)?45:25)];
    }else{
        labelScore = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",lastScore] dimensions:CGSizeMake(WIDTH_DEVICE, 50) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:((IS_IPAD)?45:25)];
    }
    labelScore.position= ccp(WIDTH_DEVICE/2, HEIGHT_DEVICE/2+HEIGHT_DEVICE/3+10);
    [self addChild:labelScore z:2];
    
    CCLabelTTF *labelHighscore = [CCLabelTTF labelWithString:@"Highscores:" dimensions:CGSizeMake(WIDTH_DEVICE, 50) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:((IS_IPAD)?40:20)];
    labelHighscore.position = ccp(WIDTH_DEVICE/2, HEIGHT_DEVICE/2+HEIGHT_DEVICE/3 - ((IS_IPAD)?50:30));
    [self addChild:labelHighscore z:2];
    

	// table view with  top scores
    float start_y = HEIGHT_DEVICE/2+HEIGHT_DEVICE/3 - ((IS_IPAD)? 80:60);
	float step = ((IS_IPAD)? 40:27);
	int count = 0;
	for(NSMutableArray *highscore in highscores) {
		NSString *player = [highscore objectAtIndex:0];
		int score = [[highscore objectAtIndex:1] intValue];
		
        CCLabelTTF *label1 = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",(count+1)] fontName:@"Marker Felt" fontSize:((IS_IPAD)?25:16)];
//        [stateTamagochi setSize:[stateTamagochi sizeTamagochi]];
		[self addChild:label1 z:5];
		[label1 setColor:ccBLACK];
		[label1 setOpacity:200];
		label1.position = ccp((backgroundTopHighscore.position.x - backgroundTopHighscore.contentSize.width/2)+20, start_y-count*step-2.0f);
        
		
        CCLabelTTF *label2 = [CCLabelTTF labelWithString:player dimensions:CGSizeMake(WIDTH_DEVICE/2, 20) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:((IS_IPAD)?25:16)];
		[self addChild:label2 z:5];
		[label2 setColor:ccBLACK];
		label2.position = ccp((backgroundTopHighscore.position.x - backgroundTopHighscore.contentSize.width/2)+WIDTH_DEVICE/3, start_y-count*step-2.0f);
        
        
		CCLabelTTF *label3 = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",score] dimensions:CGSizeMake(WIDTH_DEVICE/4, 20) hAlignment:kCCTextAlignmentRight fontName:@"Marker Felt" fontSize:((IS_IPAD)?25:16)];

		[label3 setColor:ccBLACK];
		[label3 setOpacity:200];
		label3.position = ccp((backgroundTopHighscore.position.x - backgroundTopHighscore.contentSize.width/2)+WIDTH_DEVICE/2+15, start_y-count*step-2.0f);
        [self addChild:label3 z:5];
        
		count++;
		if(count == 5) break;
	}
    
    CCMenuItem *buttonBack = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_close_highscore%@.png",MODELButtons]
                                                    selectedImage:[NSString stringWithFormat:@"btn_close_highscore%@.png",MODELButtons]
                                                           target:self selector:@selector(actionButtonBack:)];
    
    buttonBack.position = ccp(WIDTH_DEVICE/2+WIDTH_DEVICE/3+10, HEIGHT_DEVICE/2+HEIGHT_DEVICE/4);
    CCMenu *backMenu = [CCMenu menuWithItems:buttonBack, nil];
    backMenu.position = CGPointZero;
    [self addChild:backMenu z:3];
    
    
	CCMenuItem *button1 = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_play_highscore%@.png",MODELButtons]
                                                 selectedImage:[NSString stringWithFormat:@"btn_play_highscore%@.png",MODELButtons]
                                                        target:self selector:@selector(button1Callback:)];
    
	CCMenuItem *button2 = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_highscore%@.png",MODELButtons]
                                                 selectedImage:[NSString stringWithFormat:@"btn_highscore%@.png",MODELButtons]
                                                        target:self  selector:@selector(button2Callback:)];
    CCMenuItem *button3 = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_share%@.png",MODELButtons]
                                                 selectedImage:[NSString stringWithFormat:@"btn_share%@.png",MODELButtons]
                                                        target:self  selector:@selector(button3Callback:)];
    
    CCMenu *menu = [CCMenu menuWithItems: button1, button2, button3, nil];
    
    
	[menu alignItemsVerticallyWithPadding:5];
	menu.position = ccp(WIDTH_DEVICE/2,HEIGHT_DEVICE/5);

     CCLabelTTF *labelPlay = [CCLabelTTF labelWithString:@"Play" dimensions:CGSizeMake(WIDTH_DEVICE/2, 20) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:((IS_IPAD)?25:20)];
    CCLabelTTF *labelHigscore = [CCLabelTTF labelWithString:@"Change Name" dimensions:CGSizeMake(WIDTH_DEVICE/2, 20) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:((IS_IPAD)?25:20)];
    CCLabelTTF *labelShare = [CCLabelTTF labelWithString:@"Share" dimensions:CGSizeMake(WIDTH_DEVICE/2, 20) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:((IS_IPAD)?25:20)];
    [labelPlay setColor:ccWHITE];
    [labelPlay setOpacity:255];
    if (IS_IPAD) {
        
    }else{
        labelPlay.position = ccp(menu.position.x, menu.position.y+60);
    }

    [self addChild:labelPlay z:5];
    
    [labelHigscore setColor:ccWHITE];
    [labelHigscore setOpacity:255];
    labelHigscore.position = ccp(menu.position.x, menu.position.y);
    [self addChild:labelHigscore z:5];
    
    [labelShare setColor:ccWHITE];
    [labelShare setOpacity:255];
    if (IS_IPAD){
    
    }else{
    labelShare.position = ccp(menu.position.x, menu.position.y-60);
    }
    [self addChild:labelShare z:5];
	[self addChild:menu];
    
	return self;
}

- (void)dealloc {
	[highscores release];
	[super dealloc];
}

- (void)loadCurrentPlayer {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	currentPlayer = nil;
	currentPlayer = [defaults objectForKey:@"player"];
	if(!currentPlayer) {
        currentPlayer = @"Unknown Player";
	}
}

- (void)loadHighscores {	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	highscores = nil;
    
	highscores = [[NSMutableArray alloc] initWithArray: [defaults objectForKey:[NSString stringWithFormat:@"highscores%d",indexGame]]];
#ifdef RESET_DEFAULTS
	[highscores removeAllObjects];
#endif
	if([highscores count] == 0) {
		[highscores addObject:[NSArray arrayWithObjects:@"top Pou",[NSNumber numberWithInt:100],nil]];
		[highscores addObject:[NSArray arrayWithObjects:@"mr. Pou",[NSNumber numberWithInt:75],nil]];
		[highscores addObject:[NSArray arrayWithObjects:@"mrs. Pou",[NSNumber numberWithInt:50],nil]];
		[highscores addObject:[NSArray arrayWithObjects:@"Fat Pou",[NSNumber numberWithInt:25],nil]];
		[highscores addObject:[NSArray arrayWithObjects:@"BIG Pou",[NSNumber numberWithInt:10],nil]];

	}
#ifdef RESET_DEFAULTS
	[self saveHighscores];
#endif
}

- (void)updateHighscores {
    //	NSLog(@"updateHighscores");
	
	currentScorePosition = -1;
	int count = 0;
	for(NSMutableArray *highscore in highscores) {
		int score = [[highscore objectAtIndex:1] intValue];
		
		if(currentScore >= score) {
			currentScorePosition = count;
			break;
		}
		count++;
	}
	
	if(currentScorePosition >= 0) {
		[highscores insertObject:[NSArray arrayWithObjects:currentPlayer,[NSNumber numberWithInt:currentScore],nil] atIndex:currentScorePosition];
		[highscores removeLastObject];
	}
}

- (void)saveCurrentPlayer {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	[defaults setObject:currentPlayer forKey:@"player"];
}

- (void)saveHighscores {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	[defaults setObject:highscores forKey:[NSString stringWithFormat:@"highscores%d",indexGame]];
}

- (void)button1Callback:(id)sender {
    [self chooseGameAnimation];
}

- (void)chooseGameAnimation {
    
    [UIView animateWithDuration:.3
                     animations:^{
                         [[CCDirector sharedDirector] view].alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:.3
                                          animations:^{
                                              [self chooseGame];
                                          }
                                          completion:^(BOOL finished){
                                              [UIView animateWithDuration:.4
                                                               animations:^{
                                                                   [[CCDirector sharedDirector] view].alpha = 1;
                                                               }
                                                               completion:^(BOOL finished){
                                                                   
                                                               }];
                                              
                                          }];
                     }];
    
}

- (void)chooseGame{
    CCTransitionScene *ts;
    switch (indexGame) {
        case 0:
            [[[CCDirector sharedDirector] view] setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            ts = [CCTransitionFade transitionWithDuration:.1f scene:[Game scene] withColor:ccWHITE];
            break;
        case 1:
            //TODO: selectStage
            ts = [CCTransitionFade transitionWithDuration:.1f scene:[TinyPoo sceneWithStage:1] withColor:ccWHITE];
            [[[CCDirector sharedDirector] view] setTransform:CGAffineTransformMakeRotation(3*M_PI_2)];
            [[[CCDirector sharedDirector] view] setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            break;
        default:
            ts = [CCTransitionFade transitionWithDuration:1.0f scene:[Game scene] withColor:ccWHITE];
            break;
    }
	[[CCDirector sharedDirector] replaceScene:ts];
}

- (void)button2Callback:(id)sender {
    [self functionChangePlayerName];
    NSLog(@" Highscore ...");
}

- (void)button3Callback:(id)sender {
    NSLog(@" Share ...");
}

- (void)functionChangePlayerName{
	changePlayerAlert = [UIAlertView new];
	changePlayerAlert.title = @"Change Player";
	changePlayerAlert.message = @"\n";
	changePlayerAlert.delegate = self;
	[changePlayerAlert addButtonWithTitle:@"Save"];
	[changePlayerAlert addButtonWithTitle:@"Cancel"];
    
	changePlayerTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 45, 245, 27)];
	changePlayerTextField.borderStyle = UITextBorderStyleRoundedRect;
	[changePlayerAlert addSubview:changePlayerTextField];
	changePlayerTextField.keyboardType = UIKeyboardTypeDefault;
	changePlayerTextField.returnKeyType = UIReturnKeyDone;
	changePlayerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	changePlayerTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	changePlayerTextField.delegate = self;
	[changePlayerTextField becomeFirstResponder];
	[changePlayerAlert show];
}
- (void)actionButtonBack:(id)sender{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"closeGame" object:nil];
}

- (void)changePlayerDone {
	currentPlayer = [changePlayerTextField.text retain];
	[self saveCurrentPlayer];
	if(currentScorePosition >= 0) {
		[highscores removeObjectAtIndex:currentScorePosition];
		[highscores addObject:[NSArray arrayWithObjects:@"poujump",[NSNumber numberWithInt:0],nil]];
		[self saveHighscores];
		[[CCDirector sharedDirector] replaceScene:
         [CCTransitionFade transitionWithDuration:1 scene:[Highscores sceneWithScore:currentScore withIdGame:indexGame] withColor:ccWHITE]];
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	if(buttonIndex == 0) {
		[self changePlayerDone];
	} else {
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[changePlayerAlert dismissWithClickedButtonIndex:0 animated:YES];
	[self changePlayerDone];
	return YES;
}

@end
