#import "cocos2d.h"
#import "Constants.h"

#define kNumClouds			12

#define kMinPlatformStep	45
#define kNumPlatforms		10
#define kOrdCoin            10
#define kPlatformTopPadding 10

#define kMinBonusStep		30
#define kMaxBonusStep		40
#define kMinCoin            40
#define kMaxCoin            55

#define kBackgroundFirst 1111


enum {
	kSpriteManager = 0,
	kPoo,
	kScoreLabel,
    kTimerLabel,
    kBankLabel,
    kScoreCoinLabel,
	kCloudsStartTag = 100,
	kPlatformsStartTag = 200,
	kBonusStartTag = 300,
    kCoinBonusTag = 500,
    kCoinForMove = 1000,
    kMeTag,
    kBgrMenu
};

enum {
	kBonus5 = 0,
	kBonus10,
	kBonus50,
	kBonus100,
	kNumBonuses
};

enum {
    zPlatform=2,
    zTerain,
    zCloud,
    zAirplane,
    zParashute,
    zRacket,
    zAsteroid,
    zSputnic
};

@interface Main : CCLayer
{
	int currentCloudTag;
}


- (void)resetClouds;
- (void)resetCloud;
- (void)step:(ccTime)dt;
@end