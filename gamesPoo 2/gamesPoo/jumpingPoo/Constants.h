//
//  Constants.h
//  Tamagochi
//
//  Created by iVanea! on 10/9/13.
//
//

//#define IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))

#define MODEL IS_IPAD ? @"ipad" : @"iph"
#define MODELButtons IS_IPAD ? @"-iPad" : @""
#define WIDTH_DEVICE [UIScreen mainScreen].bounds.size.width
#define HEIGHT_DEVICE [UIScreen mainScreen].bounds.size.height
#define IS_IPHONE ( [[[UIDevice currentDevice] model] isEqualToString:@"iPhone"] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )
#define IS_FOUR_INCH ([UIScreen mainScreen].bounds.size.height > 480)

#define color_none 0
#define color_purple 1
#define color_yellow 2
#define color_pink 3
#define color_red 4
#define color_blue 5
#define color_green 6

#define COEF_MOVE_BACKGROUND ((IS_IPAD)? 0.05:0.2)
#define COEF_MOVE_BACKGROUND_TINY 0.1

#define kHillSegmentWidth ((IS_IPAD) ? 17 : 16)

#define RADIUS_POU ((IS_IPAD) ? 14 : 14)

#define SCALE_GAME   1.0f

#define kFPS 60

#define kFPS_Pou ((IS_IPAD) ? 60 : 55)

#define UPDATE_INTERVAL 1.0f/kFPS_Pou

#define MAX_CYCLES_PER_FRAME 8

#define SUBMULTIPLE_COIN 4

#define DOCUMENTS [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

#define PTM_RATIO 32.0

#define DISTANCE_STOP ((IS_IPAD)?700:400)

#define RADIUS_FOREACH_HERO_WHEN_STOP (RADIUS_POU*5)
#define gameFont @"Marker Felt"
enum{
    menuTag = 1,
    kBackgroundMenu=100500,
    kInformationBG=100501,
    kInformation = 100502,
    kSolution = 100503,
    kMenuButtons = 100504,
    kInstruction = 100505,
    kAlertBG,
    kInfoAlert,
    kBankMoney,
    kScore,
    kTime,
    kUmbra,
    kUmbra1,
    kUmbra2,
    kUmbra3,
    kUmbra4,
    kRabbit
};