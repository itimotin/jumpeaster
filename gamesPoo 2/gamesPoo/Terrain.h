//
//  Terrain.h
//  gamesPoo
//
//  Created by iVanea! on 6/25/13.
//  Copyright (c) 2013 iVanea!. All rights reserved.
//

#import "CCNode.h"
#import "cocos2d.h"
#import "Box2D.h"

@class TinyPoo;

@interface Terrain : CCNode

@property (retain, nonatomic) CCSprite * stripes;
@property (retain) CCSpriteBatchNode * batchNode;

- (void) setOffsetX:(float)newOffsetX;
- (id)initWithWorld:(b2World *)world;

@end

