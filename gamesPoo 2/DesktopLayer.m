//
//  DesktopLayer.m
//  StarJewel
//
//  Created by Eric Lanz on 11/23/12.
//  Copyright 2012 200 Monkeys. All rights reserved.
//

#import "DesktopLayer.h"
#import "GameLayer.h"


@implementation DesktopLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];	
	// 'layer' is an autorelease object.
	DesktopLayer *layer = [DesktopLayer node];
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}


-(void) startRound
{
    _gameScene = [CCScene node];
    GameLayer * round = [GameLayer node];
    [_gameScene addChild:round];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:_gameScene]];
}

-(void) onEnter
{
	[super onEnter];
    [self startRound];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return NO;
}
-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {}
- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {}
- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {}

@end
